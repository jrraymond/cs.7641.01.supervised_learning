import argparse
import csv
import calendar
import logging
import sqlalchemy as sql
import pandas as pd
import datetime
import orm.edgar


OUTPUT_FILE = 'sec_numbers.csv'


def get_quarter_end(dt):
    m = ((dt.month - 1)//3 + 1)*3
    d = calendar.monthrange(dt.year, m)[1]
    return datetime.date(dt.year, m, d)


def get_price_deltas(session):
    start = session.query(orm.edgar.Snp, sql.func.min(orm.edgar.Snp.date)).first()[1]
    start = get_quarter_end(start)
    end = session.query(orm.edgar.Snp, sql.func.max(orm.edgar.Snp.date)).first()[1]
    last_price = session.query(orm.edgar.Snp).filter(orm.edgar.Snp.date == start).first().close
    deltas = list()
    while start < end:
        p_date = q_end = get_quarter_end(start + datetime.timedelta(days=15))
        while True:
            price = session.query(orm.edgar.Snp).filter(orm.edgar.Snp.date == p_date).first()
            if price is not None:
                break
            p_date += datetime.timedelta(days=-1)
        deltas.append((q_end, float(price.close - last_price)))
        last_price = price.close
        start = q_end
    deltas.sort()
    return deltas


def write_price_deltas(output, deltas):
    with open(output, 'w') as fout:
        writer = csv.writer(fout)
        writer.writerow(['period', 'delta'])
        for r in deltas:
            writer.writerow(r)


def get_anl(session, periods):
    assets = list()
    liabilities = list()
    used = list()
    for d in periods:
        a = session.query(orm.edgar.Number, sql.func.sum(orm.edgar.Number.value))\
            .join(orm.edgar.Submission, orm.edgar.Number.adsh == orm.edgar.Submission.adsh)\
            .filter(orm.edgar.Number.date == d)\
            .filter(sql.or_(
                orm.edgar.Submission.form == '10-K',
                orm.edgar.Submission.form == '10-Q'))\
            .filter(orm.edgar.Number.tag == 'Assets').first()[1]
        if a is None:
            continue
        l = session.query(orm.edgar.Number, sql.func.sum(orm.edgar.Number.value)) \
            .join(orm.edgar.Submission, orm.edgar.Number.adsh == orm.edgar.Submission.adsh) \
            .filter(orm.edgar.Number.date == d)\
            .filter(sql.or_(
                orm.edgar.Submission.form == '10-K',
                orm.edgar.Submission.form == '10-Q'))\
            .filter(orm.edgar.Number.tag == 'Liabilities').first()[1]
        if l is None:
            continue
        used.append(d)
        assets.append(float(a))
        liabilities.append(float(l))
        logging.info("%s: +%s -%s", d, float(a), float(l))
    return pd.DataFrame({'period': used, 'assets': assets, 'liabilities': liabilities})


def get_most_freq_names(session):
    return session.query(orm.edgar.Submission.name, sql.func.count(orm.edgar.Submission.name))\
        .group_by(orm.edgar.Submission.name)\
        .all()


def write(database, output):
    db = 'sqlite:///{}'.format(database)
    engine = sql.create_engine(db)
    Session = sql.orm.sessionmaker(bind=engine)
    session = Session()

    deltas = get_price_deltas(session)

    df = get_anl(session, [d[0] for d in deltas])

    df = pd.concat([df, pd.DataFrame({'delta': [d[1] for d in deltas]})], axis=1).dropna()
    df.to_csv(output)


def read(fin):
    df = pd.read_csv(fin)
    p = df.period.apply(lambda dt: datetime.datetime.strptime(dt, '%Y-%m-%d').timestamp())
    x = pd.concat([p, df['assets'], df['liabilities']], axis=1)
    y = df['delta'].apply(lambda d: d >= 0)
    return x, y


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--database')
    parser.add_argument('-o', '--output', default=OUTPUT_FILE)
    return parser.parse_args()


if __name__ == '__main__':
    _args = parse_args()
    write(_args.database, _args.output)
