SEC
===

##### About The Data Set
Companies which operate in the US are required to regularly file financial statements with
the Securities and Exchange Comission (SEC). The data is made publicly available as part of the SEC's mission of
protecting investors and maintaining fair and efficient markets[1]. There a variety of reports a company may be required
to file, including the 10-Q, 10-K, 8-K, and many more. Some reports are filed every quarter or year, other reports are
filed as amendments to previous reports or when the company takes a specific action such as issueing equity or debt,
filing for bankruptcy, acquiring assets.

The 10-K and 10-Q provide quarterly comprehensive data about the performance of a company. The 10-Q is filed the first
three quarters of the year and contains unaudited view of the company's financial position throughout the year[2].
The 10-K, which is more comprehensive, is filed the fourth quarter of every year. The 10-K differs from the 10-Q in that
the financial statements are audited. The 10-Q provides a business overview,
outlines risk factors, financial details from the past five years, discussion of financial condition, and audited 
financial statements and supplementary data. Financial statements include income statements, balance sheets and cash
flows[3].

The SEC data is interesting because the data describes the state of the US economy. During a recession, companies may
have lower earnings, draw down their assets, or file for bankruptcy. When the economy is growing, more companies may
go public and file reports, or report higher earnings, or acquire smaller rivals. All this quantitative information
is present in the SEC dataset. There is also qualitative information in the form of discussion sections and letters 
to shareholders.


The income statement details how realized revenue is transformed into earnings. gains and losses. 
SalesRevenueNet, OperatingIncomeLost.

The balance sheet details the assets of the company. This assets of a company is the sum of its liabilities (what
the company owes) and shareholders' equity (net worth). Since the equation assets=liabilities+shareholders' equity will
always hold, only assets and liabilities should be included in the model. Including the third field will not add
any additional information.

The cash flow statement details the movement of cash in and out of the company. The statement gives insight into the
short-term viability of the company, in particular its ability to pay bills. Operations, investing, and financing all
impact cash flows. Cash flows from operations include transactions from operational activities. Investing includes buying
or selling equipment. Financing includes capital financing efforts (issueing equity or debt, taking out loans) or
paying out dividends and share buybacks.
PaymentsForRepurchasesOfCommonStock, PaymentsOfDividendsOfCommonStock, CashEquivalentPeriodIncreaseDecrease.

---


Decision Trees
---

Neural Networks:
---

Boosting:
---

Support Vector Machines:
---

k-Nearest Neighbors:
---

---
##### Citations
1) [Structued Disclosure at the SEC](https://www.sec.gov/page/osdhistoryandrulemaking)

2) [SEC Form 10-Q](https://www.sec.gov/fast-answers/answersform10qhtm.html)

3) [SEC Form 10-K](https://www.sec.gov/fast-answers/answers-form10khtm.html)

4) [SIC Codes](https://www.osha.gov/pls/imis/sic_manual.html)