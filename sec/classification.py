import argparse
import enum
import itertools
import typing
import logging
import random
import matplotlib.pyplot as pl
import sklearn.tree as skl_t
import sklearn.neural_network as skl_nn
import sklearn.ensemble as skl_e
import sklearn.svm as skl_svm
import sklearn.neighbors as skl_n
import sklearn.model_selection as skl_ms
import sec.preprocess
import numpy as np


class SIC(enum.Enum):
    A = 'Agriculture, Forestry and Fishing'
    B = 'Mining'
    C = 'Construction'
    D = 'Manufacturing'
    E = 'Transportation, Communications, Electric, Gas and Sanitary Services'
    F = 'Wholesale Trade'
    G = 'Retail Trade'
    H = 'Finance, Insurance and Real Estate'
    I = 'Services'
    J = 'Public Administration'
    K = 'Nonclassifiable'


_SIC_RANGES: typing.List[typing.Tuple[int, int, SIC]] = [
    (100, 999, SIC.A),
    (1000, 1499, SIC.B),
    (1500, 1799, SIC.C),
    (2000, 3999, SIC.D),
    (4000, 4999, SIC.E),
    (5000, 5199, SIC.F),
    (5200, 5999, SIC.G),
    (6000, 6799, SIC.H),
    (7000, 8999, SIC.I),
    (9100, 9729, SIC.J),
    (9900, 9999, SIC.K)
]


def get_sic(i: int) -> SIC:
    for start, stop, code in _SIC_RANGES:
        if start <= i <= stop:
            return code
    raise ValueError('Unexpected SIC {}'.format(i))


def run_classifier(classifier, inputs, outputs, num_chunks, cv=3, n_jobs=1):
    chunk_size = len(inputs) // cv * (cv - 1) // num_chunks
    chunks = [i * chunk_size for i in range(1, num_chunks+1)]
    return skl_ms.learning_curve(classifier, inputs, outputs, train_sizes=chunks, cv=cv,
                                 scoring='balanced_accuracy', n_jobs=n_jobs)


def dots_gen():
    colors = 'rgbcmyk'
    shapes = '.ovs*+xd^<>'
    dots = list(map(''.join, itertools.product(colors, shapes)))
    random.shuffle(dots)
    return dots


def plot_results(name, training_sizes, training_scores, validation_scores):
    training_scores = np.transpose(training_scores)
    validation_scores = np.transpose(validation_scores)
    pl.plot(*itertools.chain(*[(training_sizes, a, b) for a, b in zip(training_scores, dots_gen())]))
    pl.savefig('snp_{}_training_scores.png'.format(name))
    pl.cla()
    pl.clf()
    pl.plot(*itertools.chain(*[(training_sizes, a, b) for a, b in zip(validation_scores, dots_gen())]))
    pl.savefig('snp_{}_validation_scores.png'.format(name))


_CLASSIFIERS = {
    'tree': skl_t.DecisionTreeClassifier,
    'nn': skl_nn.MLPClassifier,
    'svm': lambda: skl_svm.SVC(gamma='scale'),
    'boosted': lambda: skl_e.AdaBoostClassifier(skl_t.DecisionTreeClassifier(max_depth=10)),
    'knn': skl_n.KNeighborsClassifier,
}


def main(classifiers, input, cv, chunks, n_jobs):
    inputs, is_green = sec.preprocess.read(input)
    for c in classifiers:
        clf = _CLASSIFIERS[c]()
        results = run_classifier(clf, inputs, is_green, chunks, cv, n_jobs)
        logging.info('%s:\n%s', c, '\n'.join(map(str, results)))
        plot_results(c, results[0], results[1], results[2])


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', default=sec.preprocess.OUTPUT_FILE)
    parser.add_argument('--cv', type=int, default=2)
    parser.add_argument('--chunks', type=int, default=2)
    parser.add_argument('-c', '--classifiers', nargs='*', default=list(_CLASSIFIERS.keys()))
    parser.add_argument('-j', '--jobs', type=int, default=1)
    return parser.parse_args()


if __name__ == '__main__':
    _args = parse_args()
    logging.basicConfig(level=logging.INFO)
    main(_args.classifiers, _args.input, _args.cv, _args.chunks, _args.jobs)
