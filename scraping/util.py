import os
import time
import urllib.parse
import requests


def get(url):
    response = requests.get(url)
    if response.status_code != 200:
        raise RuntimeError('request to {} returned {}'.format(url, response))
    return response


def download_file(url, dir, name):
    response = get(url)
    path = os.path.join(dir, name)
    with open(path, 'wb') as fout:
        fout.write(response.content)
    return path


def download(links, dir, log):
    average = None
    start = time.time()
    for i, link in enumerate(links):
        url = urllib.parse.urlparse(link)
        name = url.path[url.path.rfind('/') + 1:]
        try:
            download_file(link, dir, name)
            duration = time.time() - start
            if average is None:
                average = duration
            else:
                average = (average * i + duration) / (i + 1)
            time_remaining = average * (len(links) - i - 1)
            percent_done = (i + 1) / len(links) * 100
            log('{} done, {}% complete, {}s remaining'.format(name, percent_done, time_remaining))
        except RuntimeError as e:
            log(e)
        if i != len(links):
            time.sleep(2)
            start = time.time()
