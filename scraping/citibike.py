import argparse
import xml.etree.ElementTree as ET
import scraping.util

RIDE_DATA_URL = 'https://s3.amazonaws.com/tripdata'


def get_xml(url):
    response = scraping.util.get(url)
    tree = ET.fromstring(response.content)
    return tree


def get_file_names(xml):
    return [
        content.text for e in xml if 'Contents' in e.tag for content in e if 'Key' in content.tag
    ]


def main(url, dir):
    xml = get_xml(url)
    file_names = get_file_names(xml)
    print('downloading {}'.format(file_names))
    links = ['{}/{}'.format(url, name) for name in file_names]
    scraping.util.download(links, dir, print)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', default=RIDE_DATA_URL, help='Ride data root directory')
    parser.add_argument('--output-directory', default='.', help='directory to write output files')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    main(args.url, args.output_directory)
