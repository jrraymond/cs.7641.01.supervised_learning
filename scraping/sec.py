import argparse
import bs4
import urllib.parse
import scraping.util

URL = 'https://www.sec.gov/dera/data/financial-statement-data-sets.html'
TABLE_SELECTOR = '#block-secgov-content > article > div.article-content > div.content.aside.data > div.associated-data-distribution > div > div > table > tbody'


def get_links(url):
    uri = urllib.parse.urlparse(url)
    response = scraping.util.get(url)
    soup = bs4.BeautifulSoup(response.content, 'html.parser')
    body = soup.select_one(TABLE_SELECTOR)
    links = set()
    for a in body.find_all('a'):
        if 'href' in a.attrs and a.attrs['href'] not in links:
            links.add(a.attrs['href'])
    return ['{}://{}/{}'.format(uri.scheme, uri.netloc, a) for a in links]


def main(url, dir):
    links = get_links(url)
    print('Downloading {} . . '.format(links))
    scraping.util.download(links, dir, print)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', default=URL)
    parser.add_argument('-d', '--destination')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    main(args.url, args.destination)
