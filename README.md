Running the Code
---
Create a python virtual environment:

`python3.6 -m venv env`

Activate the environment:

`source env/bin/activate`

Install the dependencies:

`pip install -r requirements.txt`

Add the current directory to the `PYTHONPATH`:

`export PYTHONPATH=$PWD:$PYTHONPATH`

Run the classifiers:

````
python citibike/classification.py -i citibike_100.csv --cv 3 --chunks 3 -j 8
python sec/classification -i sec_numbers.csv
```
