import sqlalchemy.ext.declarative
import sqlalchemy as sqla
import enum


Base = sqla.ext.declarative.declarative_base()


class UserType(enum.Enum):
    Subscriber = 'Subscriber'
    Customer = 'Customer'


class Gender(enum.Enum):
    M = 'Male'
    F = 'Female'
    U = 'Unspecified'


class Station(Base):
    __tablename__ = 'station'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String)
    latitude = sqla.Column(sqla.Numeric)
    longitude = sqla.Column(sqla.Numeric)


class Trip(Base):
    __tablename__ = 'trip'
    id = sqla.Column(sqla.Integer, primary_key=True)
    duration = sqla.Column(sqla.Integer)
    start_time = sqla.Column(sqla.DateTime)
    stop_time = sqla.Column(sqla.DateTime)
    start_station_id = sqla.Column(sqla.ForeignKey(Station.id), nullable=True)
    stop_station_id = sqla.Column(sqla.ForeignKey(Station.id), nullable=True)
    bike_id = sqla.Column(sqla.Integer)
    user_type = sqla.Column(sqla.Enum(UserType), nullable=True)
    birth_year = sqla.Column(sqla.Integer)
    gender = sqla.Column(sqla.Enum(Gender))


class XTrip(Base):
    __tablename__ = 'xtrip'
    id = sqla.Column(sqla.Integer, primary_key=True)
    duration = sqla.Column(sqla.Integer)
    day = sqla.Column(sqla.Date)
    start_time = sqla.Column(sqla.Float)
    stop_time = sqla.Column(sqla.Float)
    start_latitude = sqla.Column(sqla.Float)
    start_longitude = sqla.Column(sqla.Float)
    stop_latitude = sqla.Column(sqla.Float)
    stop_longitude = sqla.Column(sqla.Float)
    bike_id = sqla.Column(sqla.Integer)
    user_type = sqla.Column(sqla.Enum(UserType), nullable=True)
    birth_year = sqla.Column(sqla.Integer)
