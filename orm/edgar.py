import sqlalchemy.ext.declarative
import sqlalchemy as sqla
import enum

Base = sqla.ext.declarative.declarative_base()


class FilerStatus(enum.Enum):
    LAF = 'Large Accelerated'
    ACC = 'Accelerated'
    SRA = 'Smaller Reporting Accelerated'
    NON = 'Non-Accelerated'
    SML = 'Smaller Reporting Filer'
    NONE = 'Not Assigned'


class Period(enum.Enum):
    FY = 0
    Q1 = 1
    Q2 = 2
    Q3 = 3
    Q4 = 4
    H1 = 5
    H2 = 6
    M9 = 7
    T1 = 8
    T2 = 9
    T3 = 10
    M8 = 11
    CY = 12


class FinancialStatement(enum.Enum):
    BS = 'Balance Sheet'
    IS = 'Income Statement'
    CF = 'Cash Flow'
    EQ = 'Equity'
    CI = 'Comprehensive Income'
    CP = 'Cover Page'
    UN = 'Unclassifiable Statement'


class File(enum.Enum):
    H = '.htm'
    X = '.xml'


class Time(enum.Enum):
    I = 'instance'
    D = 'duration'


class Balance(enum.Enum):
    C = 'credit'
    D = 'debit'


class Address(Base):
    __tablename__ = 'address'
    id = sqla.Column(sqla.Integer, primary_key=True, autoincrement=True)
    country = sqla.Column(sqla.String(2))  # ISO 3166-1 country
    state_or_province = sqla.Column(sqla.String(2), nullable=True)
    city = sqla.Column(sqla.String(30))  # city
    zip = sqla.Column(sqla.String(10), nullable=True)
    street_line1 = sqla.Column(sqla.String(40), nullable=True)
    street_line2 = sqla.Column(sqla.String(40), nullable=True)
    phone = sqla.Column(sqla.String(12), nullable=True)


class Submission(Base):
    __tablename__ = 'submission'
    adsh = sqla.Column(
        sqla.String(20), primary_key=True)  # nnnnnnnnnn-nn-nnnnnn, alphanumeric
    central_index_key = sqla.Column(sqla.String(10))  # numeric
    name = sqla.Column(sqla.String(150))
    standard_industrial_classification = sqla.Column(sqla.String(4), nullable=True)  # numeric
    business_address = sqla.Column(sqla.ForeignKey(Address.id))
    mailing_address = sqla.Column(sqla.ForeignKey(Address.id))
    country_inc = sqla.Column(sqla.String(3))
    state_or_province_inc = sqla.Column(sqla.String(2))
    irs_ein = sqla.Column(sqla.String(10), nullable=True)
    former_name = sqla.Column(sqla.String(150), nullable=True)
    name_changed = sqla.Column(sqla.Date, nullable=True)
    filer_status = sqla.Column(sqla.Enum(FilerStatus))
    wksi = sqla.Column(sqla.Boolean)
    fiscal_year = sqla.Column(sqla.Date)
    form = sqla.Column(sqla.String(20))  # submission type
    period = sqla.Column(sqla.Date)  # balance sheet date
    fiscal_year_focus = sqla.Column(sqla.Date)  # YYYY
    fiscal_period_focus = sqla.Column(sqla.Enum(Period))
    filed = sqla.Column(sqla.Date)
    accepted = sqla.Column(sqla.DateTime)
    amended = sqla.Column(sqla.Boolean)
    detail = sqla.Column(sqla.Boolean)
    instance = sqla.Column(sqla.String(32))
    nciks = sqla.Column(sqla.Integer)
    aciks = sqla.Column(sqla.String(120))


sqla.Index('submission_period', Submission.period)


class Tag(Base):
    __tablename__ = 'tag'
    # tag, version documented as keys, but a few integrity errors inserting data
    id = sqla.Column(sqla.Integer, primary_key=True, autoincrement=True)
    tag = sqla.Column(sqla.String(256))
    version = sqla.Column(sqla.String(20))
    is_custom = sqla.Column(sqla.Boolean)
    is_abstract = sqla.Column(sqla.Boolean)
    datatype = sqla.Column(sqla.String(20), nullable=True)
    time = sqla.Column(sqla.Enum(Time), nullable=True)
    balance = sqla.Column(sqla.Enum(Balance), nullable=True)
    label = sqla.Column(sqla.String(512))
    doc = sqla.Column(sqla.String(2048))

    # __table_args__ = (sqla.PrimaryKeyConstraint('tag', 'version'),)


class Number(Base):
    __tablename__ = 'number'
    id = sqla.Column(sqla.Integer, primary_key=True, autoincrement=True)
    adsh = sqla.Column(sqla.String(20), sqla.ForeignKey(Submission.adsh))
    tag = sqla.Column(sqla.String(256))
    version = sqla.Column(sqla.String(20))
    coreg = sqla.Column(sqla.String(256), nullable=True)
    date = sqla.Column(sqla.Date)
    quarters = sqla.Column(sqla.Integer)
    unit_of_measure = sqla.Column(sqla.String(20))
    value = sqla.Column(sqla.Numeric(precision=4))
    footnote = sqla.Column(sqla.String(512), nullable=True)

    __table_args__ = (sqla.UniqueConstraint('adsh', 'tag', 'version', 'coreg', 'date',
                                            'quarters', 'unit_of_measure'),)


sqla.Index('number_adsh', Number.adsh)
sqla.Index('number_date', Number.date)


class Presentation(Base):
    __tablename__ = 'presentation'
    id = sqla.Column(sqla.Integer, primary_key=True, autoincrement=True)
    adsh = sqla.Column(sqla.String(20))
    report = sqla.Column(sqla.String(6))
    line = sqla.Column(sqla.Integer)
    statement = sqla.Column(sqla.Enum(FinancialStatement))
    in_paranthetical = sqla.Column(sqla.Boolean)
    render_file = sqla.Column(sqla.Enum(File))
    tag = sqla.Column(sqla.String(256))
    version = sqla.Column(sqla.String(20))
    preferred_label = sqla.Column(sqla.String(512))

    __table_args__ = (sqla.UniqueConstraint('adsh', 'report', 'line'), )


class Snp(Base):
    __tablename__ = 'snp'
    id = sqla.Column(sqla.Integer, primary_key=True, autoincrement=True)
    date = sqla.Column(sqla.Date)
    open = sqla.Column(sqla.Numeric(precision=2))
    high = sqla.Column(sqla.Numeric(precision=2))
    low = sqla.Column(sqla.Numeric(precision=2))
    close = sqla.Column(sqla.Numeric(precision=2))
    adj_close = sqla.Column(sqla.Numeric(precision=2))
    volume = sqla.Column(sqla.Integer)

    __table_args__ = (sqla.UniqueConstraint('date'),)


sqla.Index('snp_date', Snp.date)
