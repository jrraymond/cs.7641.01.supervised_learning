import argparse
import itertools
import logging
import random
import numpy as np
import matplotlib.pyplot as pl
import sklearn.tree as skl_t
import sklearn.neural_network as skl_nn
import sklearn.ensemble as skl_e
import sklearn.svm as skl_svm
import sklearn.neighbors as skl_n
import sklearn.model_selection as skl_ms
import citibike.preprocess as preprocess


def run_classifier(classifier, inputs, outputs, num_chunks, cv=3, n_jobs=1):
    chunk_size = len(inputs) // cv * (cv - 1) // num_chunks
    chunks = [i * chunk_size for i in range(1, num_chunks+1)]
    return skl_ms.learning_curve(classifier, inputs, outputs, train_sizes=chunks, cv=cv,
                                 scoring='balanced_accuracy', n_jobs=n_jobs)


def dots_gen():
    colors = 'rgbcmyk'
    shapes = '.ovs*+xd^<>'
    dots = list(map(''.join, itertools.product(colors, shapes)))
    random.shuffle(dots)
    return dots


def plot_results(name, training_sizes, training_scores, validation_scores):
    training_scores = np.transpose(training_scores)
    validation_scores = np.transpose(validation_scores)
    pl.plot(*itertools.chain(*[(training_sizes, a, b) for a, b in zip(training_scores, dots_gen())]))
    pl.savefig('{}_training_scores.png'.format(name))
    pl.cla()
    pl.clf()
    pl.plot(*itertools.chain(*[(training_sizes, a, b) for a, b in zip(validation_scores, dots_gen())]))
    pl.savefig('{}_validation_scores.png'.format(name))


_CLASSIFIERS = {
    'tree': skl_t.DecisionTreeClassifier,
    'nn': skl_nn.MLPClassifier,
    'svm': lambda: skl_svm.SVC(gamma='scale'),
    'boosted': lambda: skl_e.AdaBoostClassifier(skl_t.DecisionTreeClassifier(max_depth=10)),
    'knn': skl_n.KNeighborsClassifier,
}


def main(classifiers, input, cv, chunks, n_jobs):
    inputs, is_holiday = preprocess.read(input)
    for c in classifiers:
        clf = _CLASSIFIERS[c]()
        results = run_classifier(clf, inputs, is_holiday, chunks, cv, n_jobs)
        logging.info('%s:\n%s', c, '\n'.join(map(str, results)))
        plot_results(c, results[0], results[1], results[2])


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', default=preprocess.OUTPUT_FILE)
    parser.add_argument('--cv', type=int, default=3)
    parser.add_argument('--chunks', type=int, default=3)
    parser.add_argument('-c', '--classifiers', nargs='*', default=list(_CLASSIFIERS.keys()))
    parser.add_argument('-j', '--jobs', type=int)
    return parser.parse_args()


if __name__ == '__main__':
    _args = parse_args()
    logging.basicConfig(level=logging.INFO)
    main(_args.classifiers, _args.input, _args.cv, _args.chunks, _args.jobs)
