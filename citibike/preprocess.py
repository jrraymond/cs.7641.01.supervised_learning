import argparse

import pandas as pd
import sqlalchemy as sql
from pandas.tseries import holiday as pd_holiday

import orm.cb


OUTPUT_FILE = 'citibike.csv'


def write(db, sample, csv):
    db = 'sqlite:///{}'.format(db)
    engine = sql.create_engine(db)
    Session = sql.orm.sessionmaker(bind=engine)
    session = Session()
    trips = pd.read_sql(session.query(orm.cb.XTrip).filter(orm.cb.XTrip.id % sample == 0).statement, session.bind)\
        .dropna()

    holidays = pd_holiday.USFederalHolidayCalendar().holidays()
    is_holiday = trips['day'].apply(lambda d: d.weekday() >= 5 or d in holidays)

    user_type = pd.get_dummies(trips['user_type'])
    inputs = trips[['duration', 'bike_id', 'birth_year', 'start_latitude', 'start_longitude', 'stop_latitude',
                    'stop_longitude', 'start_time', 'stop_time']]
    df = pd.concat([inputs, user_type, is_holiday], axis=1)
    df.to_csv(csv)


def read(file):
    df = pd.read_csv(file)
    inputs = df[['duration', 'bike_id', 'birth_year', 'start_latitude', 'start_longitude', 'stop_latitude',
                 'stop_longitude', 'UserType.Customer', 'UserType.Subscriber', 'start_time', 'stop_time']]
    outputs = df['day']
    return inputs, outputs


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--database', default='citibikex.db')
    parser.add_argument('-s', '--sample', type=int, default=1000)
    parser.add_argument('-o', '--output', default=OUTPUT_FILE)
    return parser.parse_args()


if __name__ == '__main__':
    _args = parse_args()
    write(_args.database, _args.sample, _args.output)
