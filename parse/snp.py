import orm.edgar as edgar
import logging
import sqlalchemy
import csv
import traceback
import argparse
import datetime
import parse.util


def parse_row(session, row: parse.util.Row):
    price = edgar.Snp(
        date=datetime.datetime.strptime(row['date'], '%Y-%m-%d').date(),
        open=float(row['open']),
        high=float(row['high']),
        low=float(row['low']),
        close=float(row['close']),
        adj_close=float(row['adj close']),
        volume=int(row['volume'])
    )
    session.add(price)


def main(data, database):
    engine = sqlalchemy.create_engine('sqlite:///{}'.format(database))
    try:
        edgar.Snp.__table__.drop(engine)
    except sqlalchemy.exc.OperationalError:
        pass
    edgar.Snp.__table__.create(engine)
    Session = sqlalchemy.orm.sessionmaker(bind=engine)
    session = Session()
    with open(data) as fin:
        reader = csv.reader(fin)
        header = dict((c.lower(), i) for i, c in enumerate(c.strip() for c in next(reader)))
        for i, line in enumerate(reader):
            row = parse.util.Row(header, line, i+2, data)
            try:
                parse_row(session, row)
            except RuntimeError:
                msg = 'Exception processing {}:\n{}'.format(row, traceback.format_exc())
                logging.info(msg)
    session.commit()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', required=True)
    parser.add_argument('--database', required=True)
    parser.add_argument('--log', default='{}.log'.format(__file__))
    parser.add_argument('--verbosity', type=int, default=logging.INFO)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    logging.basicConfig(filename=args.log, level=args.verbosity)
    main(args.data, args.database)
