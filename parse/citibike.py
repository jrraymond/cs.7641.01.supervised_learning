import argparse
import csv
import decimal
import datetime
import os
import subprocess
import shutil
import traceback
import time
import uuid
import sqlalchemy
import sqlalchemy.orm
import logging
import typing

from parse.util import get_zipped_files, extract, consume, Row
import orm.cb


def parse_datetime(s: str) -> datetime.datetime:
    formats = ('%Y-%m-%d %H:%M:%S.%f', '%Y-%m-%d %H:%M:%S', '%m/%d/%Y %H:%M:%S', '%m/%d/%Y %H:%M')
    for f in formats:
        try:
            return datetime.datetime.strptime(s, f)
        except ValueError:
            pass
    raise ValueError('Datetime "{}" could not be parsed as one of {}'.format(s, formats))


def parse_stations(row: Row) -> typing.Tuple[orm.cb.Station, orm.cb.Station]:
    start = stop = None
    start_id = row['trip.start_station_id']
    if start_id != 'NULL':
        start = orm.cb.Station(id=int(start_id),
                               name=row['trip.start_station_name'],
                               latitude=decimal.Decimal(row['trip.start_station_latitude']),
                               longitude=decimal.Decimal(row['trip.start_station_longitude']))
    stop_id = row['trip.stop_station_id']
    if stop_id != 'NULL':
        stop = orm.cb.Station(id=int(stop_id),
                              name=row['trip.stop_station_name'],
                              latitude=decimal.Decimal(row['trip.stop_station_latitude']),
                              longitude=decimal.Decimal(row['trip.stop_station_longitude']))
    return start, stop


def parse_gender(s: str) -> orm.cb.Gender:
    if s == '1':
        return orm.cb.Gender.M
    elif s == '2':
        return orm.cb.Gender.F
    else:
        return orm.cb.Gender.U


def parse_birth_year(by: str) -> typing.Union[None, int]:
    try:
        return int(by)
    except ValueError:
        return None


def parse_trip(row: Row, start_station: int, stop_station: int) -> orm.cb.Trip:
    trip = orm.cb.Trip(id=row['trip.id'],
                       duration=int(row['trip.duration']),
                       start_time=parse_datetime(row['trip.start_time']),
                       stop_time=parse_datetime(row['trip.stop_time']),
                       start_station_id=start_station,
                       stop_station_id=stop_station,
                       bike_id=int(row['trip.bike_id']),
                       user_type=orm.cb.UserType[row['trip.user_type']] if row['trip.user_type'] else None,
                       birth_year=parse_birth_year(row['trip.birth_year']),
                       gender=parse_gender(row['trp.gender']))
    return trip


COLUMN_MAP = {
    'id': 'trip.id',
    'trip duration': 'trip.duration',
    'tripduration': 'trip.duration',
    'start time': 'trip.start_time',
    'starttime': 'trip.start_time',
    'stop time': 'trip.stop_time',
    'stoptime': 'trip.stop_time',
    'start station id': 'trip.start_station_id',
    'start station name': 'trip.start_station_name',
    'start station latitude': 'trip.start_station_latitude',
    'start station longitude': 'trip.start_station_longitude',
    'end station id': 'trip.stop_station_id',
    'end station name': 'trip.stop_station_name',
    'end station latitude': 'trip.stop_station_latitude',
    'end station longitude': 'trip.stop_station_longitude',
    'bike id': 'trip.bike_id',
    'bikeid': 'trip.bike_id',
    'user type': 'trip.user_type',
    'usertype': 'trip.user_type',
    'birth year': 'trip.birth_year',
    'gender': 'trip.gender',
}


def parse_header(fieldnames):
    column_map = dict()
    for i, field in enumerate(fieldnames):
        field = field.lower()
        if field not in COLUMN_MAP:
            raise RuntimeError('Unmapped column "{}"'.format(field))
        column = COLUMN_MAP[field]
        column_map[column] = i
    return column_map


def parse_csv(Session, file):
    start_time = time.time()
    session = Session()
    reader = csv.reader(file)
    column_map = parse_header(next(reader))
    for i, row in enumerate(reader):
        row = Row(column_map, row, i+2, file)
        try:
            start, stop = parse_stations(row)
            for station in filter(bool, (start, stop)):
                exists = session.query(orm.cb.Station).get(station.id)
                if not exists:
                    session.add(station)
            session.flush()
            trip = parse_trip(row, start.id if start else None, stop.id if stop else None)
            session.add(trip)
        except Exception:
            logging.warning('Failed to parse row {}: \n{}'.format(row, traceback.format_exc()))
    session.commit()
    duration = time.time() - start_time
    logging.info('. . . inserted {} rows in {}s'.format(i, duration))


def insert_data(Session, csvs):
    for csv_file in csvs:
        logging.info('. . . parsing {} . . '.format(csv_file))
        with open(csv_file) as fin:
            try:
                parse_csv(Session, fin)
            except Exception:
                logging.info(' . . . skipping because %s', traceback.format_exc())


def _to_seconds(dt: datetime) -> float:
    return (datetime.datetime.combine(datetime.date.min, dt.time()) - datetime.datetime.min).total_seconds()


def parse_xtrip(row):
    start_dt = parse_datetime(row['trip.start_time'])
    start_time = _to_seconds(start_dt)
    stop_time = _to_seconds(parse_datetime(row['trip.stop_time']))
    return orm.cb.XTrip(id=row['trip.id'],
                        duration=int(row['trip.duration']),
                        day=start_dt.date(),
                        start_time=start_time,
                        stop_time=stop_time,
                        start_latitude=float(row['trip.start_station_latitude']),
                        start_longitude=float(row['trip.start_station_longitude']),
                        stop_latitude=float(row['trip.stop_station_latitude']),
                        stop_longitude=float(row['trip.stop_station_longitude']),
                        bike_id=int(row['trip.bike_id']),
                        user_type=orm.cb.UserType[row['trip.user_type']] if row['trip.user_type'] else None,
                        birth_year=parse_birth_year(row['trip.birth_year']))


def flat_parse_csv(Session, file):
    start_time = time.time()
    session = Session()
    reader = csv.reader(file)
    column_map = parse_header(next(reader))
    for i, row in enumerate(reader):
        row = Row(column_map, row, i+2, file)
        try:
            xtrip = parse_xtrip(row)
            session.add(xtrip)
        except Exception:
            logging.warning('Failed to parse row {}: \n{}'.format(row, traceback.format_exc()))
        if i % 1000000 == 0:
            session.commit()
    session.commit()
    duration = time.time() - start_time
    logging.info('. . . inserted {} rows in {}s'.format(i, duration))


def flat_consumer(Session, csvs):
    for csv_file in csvs:
        logging.info('. . . parsing {} . . '.format(csv_file))
        with open(csv_file) as fin:
            try:
                flat_parse_csv(Session, fin)
            except Exception:
                logging.info(' . . . skipping because %s', traceback.format_exc())


def main(data_dir, database, flat):
    engine = sqlalchemy.create_engine('sqlite:////{}'.format(database))
    orm.cb.Base.metadata.create_all(engine)
    Session = sqlalchemy.orm.sessionmaker(bind=engine)

    if flat:
        consumer = lambda f: flat_consumer(Session, f)
    else:
        def consumer(files):
            insert_data(Session, files)

    consume(data_dir, consumer)


def validate(data_dir):
    tmp_dir = os.path.join(data_dir, 'tmp')
    rm = input('Ok to remove {}? (Y/n)'.format(tmp_dir))
    if rm and rm not in 'Yy':
        return
    total = 0
    for file in get_zipped_files(data_dir):
        dir = os.path.join(tmp_dir, str(uuid.uuid4()))
        os.makedirs(dir)
        extracted = extract(file, dir)
        for extracted in filter(lambda f: f.endswith('.csv'), extracted):
            process = subprocess.Popen(['wc', '-l', extracted],
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            stdout, stderr = tuple(map(lambda b: b.decode('utf-8'), process.communicate()))
            if process.returncode != 0:
                print('wc {} failed with {}: {}\n{}'.format(extracted, process.returncode, stdout,
                                                            stderr))
                continue
            print('{}'.format(stdout))
            try:
                total += int(stdout[:stdout.find(' ')])
            except ValueError as e:
                print(e)
                pass
        shutil.rmtree(dir, ignore_errors=True)
    print(total)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', help='directory containing data')
    parser.add_argument('--database', help='name of database', default='citibike.db')
    parser.add_argument('--validate', help='Estimate number of rows', action='store_true')
    parser.add_argument('--log', default='{}.log'.format(__file__))
    parser.add_argument('--verbosity', type=int, default=logging.INFO)
    parser.add_argument('-f', '--flat', action='store_true')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    logging.basicConfig(filename=args.log, level=args.verbosity)
    if args.validate:
        validate(args.data)
    else:
        main(args.data, args.database, args.flat)
