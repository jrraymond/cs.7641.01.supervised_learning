import argparse
import decimal
import os
import csv
import logging
import traceback
import parse.util
import sqlalchemy
import sqlalchemy.orm
import datetime
from dateutil.relativedelta import relativedelta
from typing import List, Union
import orm.edgar as edgar
from parse.util import Row


def parse_business_address(row: Row) -> edgar.Address:
    return edgar.Address(
        country=row['countryba'],
        state_or_province=row['stprba'],
        city=row['cityba'],
        zip=row['zipba'],
        street_line1=row['bas1'],
        street_line2=row['bas2'],
        phone=row['baph'])


def parse_mailing_address(row: Row) -> edgar.Address:
    return edgar.Address(
        country=row['countryma'],
        state_or_province=row['stprma'],
        city=row['cityma'],
        zip=row['zipma'],
        street_line1=row['mas1'],
        street_line2=row['mas2'],
        phone=None)


def parse_period(p: str) -> datetime.date:
    p = edgar.Period[p]
    yesterday = relativedelta(days=-1)
    if p == edgar.Period.Q1:
        return datetime.date(1, 4, 1) + yesterday
    elif p == edgar.Period.Q2:
        return datetime.date(1, 8, 1) + yesterday
    elif p == edgar.Period.Q3:
        return datetime.date(2, 1, 1) + yesterday
    raise ValueError('Unhandled period {}'.format(p))


def parse_date(s: str, formats: List[str]) -> datetime.date:
    for f in formats:
        try:
            return datetime.datetime.strptime(s, f).date() if f != '%p' else parse_period(s)
        except ValueError:
            pass
    raise ValueError('Date "{}" could not be parsed as one of {}'.format(s, formats))


def parse_optional_date(s: str, formats: List[str]) -> Union[None, datetime.date]:
    if s == '':
        return None
    return parse_date(s, formats)


def parse_optional_md(s: str) -> Union[None, datetime.date]:
    if s == '':
        return None
    return datetime.datetime.strptime('0004' + s, '%Y%m%d').date()


def parse_filer_status(s: str) -> edgar.FilerStatus:
    i = s.find('-')
    if i < 0:
        return edgar.FilerStatus.NONE
    return edgar.FilerStatus[s[i+1:]]


def parse_submission(row: Row, ba: edgar.Address, ma: edgar.Address) -> edgar.Submission:
    return edgar.Submission(
        adsh=row['adsh'],
        central_index_key=row['cik'],
        name=row['name'],
        standard_industrial_classification=row['sic'],
        business_address=ba.id,
        mailing_address=ma.id,
        country_inc=row['countryinc'],
        state_or_province_inc=row['stprinc'],
        irs_ein=row['ein'],
        former_name=row['former'],
        name_changed=parse_optional_date(row['changed'], ['%Y%m%d']),
        filer_status=parse_filer_status(row['afs']),
        wksi=row['wksi'] == '1',
        fiscal_year=parse_optional_md(row['fye']),
        form=row['form'],
        period=parse_date(row['period'], ['%Y%m%d']),
        fiscal_year_focus=parse_optional_date(row['fy'], ['%Y']),
        fiscal_period_focus=None if not row['fp'] else edgar.Period[row['fp']],
        filed=datetime.datetime.strptime(row['filed'], '%Y%m%d').date(),
        accepted=datetime.datetime.strptime(row['accepted'], '%Y-%m-%d %H:%M:%S.%f'),
        amended=row['prevrpt'] == '1',
        detail=row['detail'] == '1',
        instance=row['instance'],
        nciks=int(row['nciks']),
        aciks=row['aciks'])


def parse_sub_row(session, row: Row):
    try:
        ba = parse_business_address(row)
        ma = parse_mailing_address(row)
        sub = parse_submission(row, ba, ma)
        session.add_all((ba, ma, sub))
    except (KeyError, ValueError):
        msg = 'Failed to parse row {} \n{}'.format(row, traceback.format_exc())
        logging.warning(msg)
    return


def parse_num_row(session, row: Row):
    try:
        num = edgar.Number(
            adsh=row['adsh'],
            tag=row['tag'],
            version=row['version'],
            coreg=row['coreg'],
            date=datetime.datetime.strptime(row['ddate'], '%Y%m%d'),
            quarters=int(row['qtrs']),
            unit_of_measure=row['uom'],
            value=decimal.Decimal(row['value']) if row['value'] else decimal.Decimal(),
            footnote=row['footnote']
        )
        session.add(num)
    except decimal.InvalidOperation :
        msg = 'Failed to parse row {} \n{}'.format(row, traceback.format_exc())
        logging.warning(msg)


def parse_tag_row(session, row: Row):
    tag = edgar.Tag(
        tag=row['tag'],
        version=row['version'],
        is_custom=row['custom'] == '1',
        is_abstract=row['abstract'] == '1',
        datatype=row['datatype'],
        time=edgar.Time[row['iord']] if row['iord'] else None,
        balance=edgar.Balance[row['crdr']] if row['crdr'] else None,
        label=row['tlabel'],
        doc=row['doc']
    )
    session.add(tag)


def parse_pre_row(session, row: Row):
    pre = edgar.Presentation(
        adsh=row['adsh'],
        report=row['report'],
        line=int(row['line']),
        statement=edgar.FinancialStatement[row['stmt']],
        in_paranthetical=row['inpth'] == '1',
        render_file=edgar.File[row['rfile']],
        tag=row['tag'],
        version=row['version'],
        preferred_label=row['plabel']
    )
    session.add(pre)


def insert(session, file, parse_fn):
    with open(file, encoding='latin-1') as fin:
        try:
            reader = csv.reader(fin, delimiter='\t', quoting=csv.QUOTE_NONE)
            header = dict(
                (c, i) for i, c in enumerate(c.strip() for c in next(reader) if c.strip() != ''))
            for i, line in enumerate(reader):
                row = Row(header, line, i + 2, file)
                parse_fn(session, row)
                if i % 10000 == 0:
                    session.commit()
                    logging.info('{} rows from {} inserted'.format(i, file))
            session.commit()
            logging.info('{} rows from {} inserted'.format(i, file))
        except UnicodeDecodeError:
            msg = 'Error reading {}:\n{}'.format(file, traceback.format_exc())
            logging.error(msg)
        except sqlalchemy.exc.IntegrityError:
            msg = 'Error inserting data {}:\n{}'.format(file, traceback.format_exc())
            logging.error(msg)


def insert_sub(session, file: str):
    insert(session, file, parse_sub_row)


def insert_num(session, file: str):
    insert(session, file, parse_num_row)


def insert_pre(session, file: str):
    insert(session, file, parse_pre_row)


def insert_tag(session, file: str):
    insert(session, file, parse_tag_row)


def insert_data(Session, files):
    logging.info('Inserting data from {}'.format(files))
    session = Session()

    submission_file = [f for f in files if os.path.basename(f) == 'sub.txt'][0]
    insert_sub(session, submission_file)

    tag_file = [f for f in files if os.path.basename(f) == 'tag.txt'][0]
    insert_tag(session, tag_file)

    numbers_file = [f for f in files if os.path.basename(f) == 'num.txt'][0]
    insert_num(session, numbers_file)

    presentation_file = [f for f in files if os.path.basename(f) == 'pre.txt'][0]
    insert_pre(session, presentation_file)


def _drop(engine, table):
    try:
        table.__table__.drop(bind=engine)
    except sqlalchemy.exc.OperationalError:
        pass


def main(data_dir, database):
    engine = sqlalchemy.create_engine('sqlite:////{}'.format(database))

    for table in (edgar.Address, edgar.Presentation, edgar.Tag, edgar.Number, edgar.Submission):
        _drop(engine, table)
        table.__table__.create(bind=engine)

    Session = sqlalchemy.orm.sessionmaker(bind=engine)

    def consumer(files):
        insert_data(Session, list(files))

    parse.util.consume(data_dir, consumer)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data-dir', required=True)
    parser.add_argument('--database', required=True)
    parser.add_argument('--log', default='{}.log'.format(__file__))
    parser.add_argument('--verbosity', type=int, default=logging.INFO)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    logging.basicConfig(filename=args.log, level=args.verbosity)
    main(args.data_dir, args.database)
