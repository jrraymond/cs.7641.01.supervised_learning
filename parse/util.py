import glob
import os
import shutil
import logging
import time
import traceback
import zipfile
from typing import Dict, List, Union


def get_zipped_files(dir):
    path = os.path.join(dir, '*.zip')
    yield from glob.iglob(path)


def extract(zip_file, destination):
    with zipfile.ZipFile(zip_file, 'r') as zip:
        zip.extractall(destination)
        yield from glob.iglob(os.path.join(destination, '*'))


def consume(data_dir, consumer):
    tmp_dir = os.path.join(data_dir, 'tmp')
    rm = input('Ok to remove {}? (Y/n)'.format(tmp_dir))
    if rm and rm not in 'Yy':
        return
    start = time.time()
    shutil.rmtree(tmp_dir, ignore_errors=True)
    for file in get_zipped_files(data_dir):
        os.makedirs(tmp_dir)
        process(consumer, tmp_dir, file)
        shutil.rmtree(tmp_dir, ignore_errors=True)
    logging.info('. . . done in {}s'.format(time.time() - start))


def process(consumer, tmp_dir, file):
    try:
        logging.info('Processing {}'.format(file))
        extracted = extract(file, tmp_dir)
        consumer(extracted)
        logging.info('. . . {} done'.format(file))
    except Exception as e:
        logging.info('. . . failed {}\n{}'.format(e, traceback.format_exc()))


class Row:
    def __init__(self, header: Dict[str, int], row: List[str], index: int, file: str):
        self._header = header
        self._row = row
        self._file = file
        self._index = index

    def __getitem__(self, key: str) -> Union[None, str]:
        i = self._header.get(key)
        if i is None or i >= len(self._row):
            return None
        return self._row[i]

    def __repr__(self):
        return str('{}:{}:{}'.format(self._file, self._index, dict(zip(self._header, self._row))))